#Adversarial Search#

###1. Two agents are to play the board game Fruit Rage (based on Candy Crush).
###2. The objective of the game is to gain maximum points as much as possible on each move.
###3. I developed the game playing agent using Minimax and Alpha-beta pruning to find the next optimal move for the player.  